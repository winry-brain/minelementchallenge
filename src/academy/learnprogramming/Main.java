package academy.learnprogramming;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
//        System.out.println(getCount());
//        System.out.println(readIntegers(5));
        System.out.println(readIntegers(getCount()));

    }

    public static int getCount() {

        Scanner countScanner = new Scanner(System.in);
        int count;

        while (true) {
            System.out.println("Enter number of elements in the array");
            boolean isAnInt = countScanner.hasNextInt();
            if (isAnInt) {
                count = countScanner.nextInt();
                if (count > 0) break;
                else System.out.println("Entered value is less or equal to zero");
            } else {
                System.out.println("You entered invalid value. Make sure that it's a whole digit");
                countScanner.next();
            }
            countScanner.close();
        }

        return count;

    }

    //    @NotNull
    public static int[] readIntegers(int count) {

        Scanner arrayScanner = new Scanner(System.in);
        int i = 0;
        int[] array = new int[count];

        while (true) {

            System.out.println("Enter an array[" + i + "] element");
            boolean isAnInt = arrayScanner.hasNextInt();

            if (isAnInt) {
            array[i] = arrayScanner.nextInt();
            arrayScanner.nextLine();
            i++;
            } else {
                System.out.println("Invalid value");
                arrayScanner.next();
            }

            if (i == count) break;
        }
        arrayScanner.close();
        System.out.println("You added following numbers in the array " + Arrays.toString(array));
        return array;
    }

    public static int findMin(int[] array) {
        return 1;
    }
//    }
}


//        -Write a method findMin() with the array as a parameter. The method needs to return the minimum value in the array.
//
//        -Then call the findMin() method passing the array returned from the call to the readIntegers() method.
//
//        -Finally, print the minimum element in the array.
//
//        Tips:
//        -Assume that the user will only enter numbers, never letters
//        -For simplicity, create a Scanner as a static field to help with data input
//        -Create a new console project with the name eMinElementChallengef
